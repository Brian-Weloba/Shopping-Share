# Shopping-Share - Android App !

Shopping share is a b2c service that allows users to purchase daily groceries remotely thus reducing time spent in stores.

## About

Shopping share allows users to make purchases online and have their items ready for pickup before they arrive. This will help them save time spent buyong the same daily consumer items.

Shopping-Share is a creation from Brian Weloba released under the MIT License (Expat License).

## Features

The android app lets you:
- Place orders to supermarkets online..
- Add and remove items from your daily cart
- Completely ad-free.
- Create custom carts.
- Needs no special permissions on Android 6.0+.

## Screenshots

*

##Technologies

- <img alt="Material UI" src="https://img.shields.io/badge/materialui-%230081CB.svg?style=for-the-badge&logo=material-ui&logoColor=white"/>
- <img alt="Java" src="https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white"/>
- <img alt="Figma" src="https://img.shields.io/badge/figma-%23F24E1E.svg?style=for-the-badge&logo=figma&logoColor=white"/>
- <img alt="Git" src="https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white"/>

## Permissions

- Full Network Access.
- View Network Connections.

## Contributing

wallabag app is a free and open source project. Any contributions are welcome. Here are a few ways you can help:
 * [Report bugs and make suggestions.](https://github.com/Brian-Weloba/Shopping-Share/issues)
 * Write some code. Please follow the code style used in the project to make a review process faster.

## License
